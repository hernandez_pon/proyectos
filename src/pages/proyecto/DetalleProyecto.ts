import { Component } from '@angular/core'
import { NavParams } from 'ionic-angular';
import { Proyecto } from '../../interfaces/proyecto';

@Component({
	selector: 'page-detalle-proyecto',
	templateUrl: 'detalle-proyecto.html'
})

export class DetalleProyectoPage {
	proyecto = {};
	constructor(private navParams: NavParams) {
		this.proyecto = navParams.get('id');
	}
}
