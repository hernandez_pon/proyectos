import { Component, OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ProyectoService } from '../../services/proyecto.service';
import { Proyecto } from '../../interfaces/proyecto';
import { DetalleProyectoPage } from './DetalleProyecto';
import { PROYECTOS } from '../../services/mocks/proyectos';

@Component({
	selector: 'page-proyecto',
	templateUrl: 'proyecto.html'
})

/* Clase de mi componente proyecto.html */
export class ProyectoPage implements OnInit {

	proyectos = [];
	items = [];

	ngOnInit(): void {
		this.getProyectos();
	}

	constructor(
		public navCtrl: NavController,
		private proyectoService: ProyectoService) {
	}

	/* Obtenemos los proyectos del servicio de proyectos. */
	getProyectos = (): void  => {
		this.proyectoService.getProyectos()
		.then(proyectos => this.proyectos = proyectos);	
	}

	/* Funcion para ver el detalle de un proyecto. */
	detalleProyecto = (_proyecto: Proyecto): void => {
		this.navCtrl.push(DetalleProyectoPage, {
			id: _proyecto
		});
	}

	/* Funcion para filtar los proyectos. */
	buscaProyectos = (event: any): void => {
		// Obtenemos el valor del input.
		let val = event.target.value;

		// Si el valor no es vacio filtra los proyectos.
		if(val && val.trim() != '') {
			this.proyectos = this.proyectoService.filtrarProyectos(val)
			.then(items => this.items = items)
			.then(() => this.proyectos = this.proyectoService.muestraProyecto(this.items, val))
		}
		/* Si no hay ningun valor en el campo muestra el listado de los proyectos. */
		else {
			this.getProyectos();
		}
	}
}
