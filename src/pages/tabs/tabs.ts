import { Component } from '@angular/core';

import { AboutPage } from '../about/about';
import { ContactPage } from '../contact/contact';
import { ProyectoPage } from '../proyecto/proyecto';

@Component({
	templateUrl: 'tabs.html'
})
export class TabsPage {

	tab1Root = ProyectoPage;
	tab2Root = AboutPage;
	tab3Root = ContactPage;

	constructor() {

	}
}
