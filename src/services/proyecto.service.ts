import { Injectable } from '@angular/core'
import 'rxjs/add/operator/map';
import { Http } from '@angular/http';

import { Proyecto } from '../interfaces/proyecto';
import { PROYECTOS } from '../services/mocks/proyectos';

/* Clase para la funcionalidad de mi componenete proyectos. */
@Injectable()
export class ProyectoService {

	/* Obtenemos los proyectos. */
	getProyectos = (): Promise<Proyecto[]>  => {
		return Promise.resolve(PROYECTOS);
	}

	/* Funcion para obtener los proyectos. */
	filtrarProyectos = (val): any => {
		/* Obtenemos todos datos del proyecto. */
		return Promise.resolve(PROYECTOS);
	}

	/* Funcion para filtrar los proyectos dado a un valor. */
	muestraProyecto (arreglo, val) {
		// filtramos el proyecto y devolvemos los datos.
		return arreglo.filter((item) => {
			return (item.nombre.toLowerCase().indexOf(val.toLowerCase()) > -1);
		});
	}
}