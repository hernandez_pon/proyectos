export interface Proyecto {
	id: number;
	nombre: string;
	monto: string;
	moneda: string;
	gerencia: string;
	pais: string;
	unidad_negocio: string;
	producto: string;
	anio: number;
	contratante: string;
	datos_cliente: string;
	fecha_inicio: string;
	fecha_fin: string;
	duracion: string;
	numero_contrato: number;
	anticipo: string;
	costo_directo: string;
	numero_propuesta: string;
}